import { useEffect, useState } from "react";
import "./RgbPanel.css";

const RgbPanel = ({ getData, status, updateStatus, setPreColor }) => {
  const [hexR, setHexR] = useState("0");
  const [hexG, setHexG] = useState("0");
  const [hexB, setHexB] = useState("0");
  const [hex, setHex] = useState();

  function setColor() {
    let square = document.querySelector("#color-square");
    let r = document.querySelector("#r");
    let g = document.querySelector("#g");
    let b = document.querySelector("#b");

    let r_hex = parseInt(r.value, 10).toString(16);
    let g_hex = parseInt(g.value, 10).toString(16);
    let b_hex = parseInt(b.value, 10).toString(16);
    let hex = "#" + pad(r_hex) + pad(g_hex) + pad(b_hex);
    square.style.backgroundColor = hex;
    setHex(hex);
  }

  function pad(n) {
    return n.length < 2 ? "0" + n : n;
  }

  useEffect(() => {
    if (status) {
      setColor();
    }
  });

  const changeHexR = () => {
    setHexR(document.querySelector("#r").value);
  };
  const changeHexG = () => {
    setHexG(document.querySelector("#g").value);
  };
  const changeHexB = () => {
    setHexB(document.querySelector("#b").value);
  };

  return (
    <div className="rgb-panel-area">
      {status ? (
        <div>
          <fieldset>
            <label htmlFor="r">R</label>
            <input
              type="range"
              min="0"
              max="255"
              id="r"
              step="1"
              value={hexR}
              onChange={changeHexR}
            />
          </fieldset>

          <fieldset>
            <label htmlFor="g">G</label>
            <input
              type="range"
              min="0"
              max="255"
              id="g"
              step="1"
              value={hexG}
              onChange={changeHexG}
            />
          </fieldset>

          <fieldset>
            <label htmlFor="b">B</label>
            <input
              type="range"
              min="0"
              max="255"
              id="b"
              step="1"
              value={hexB}
              onChange={changeHexB}
            />
          </fieldset>
          <div className="buttons-panel">
            <button
              onClick={(e) => {
                e.preventDefault();
                getData(hex);
                updateStatus(false);
              }}
              className="rgb-button"
            >
              Save color
            </button>
            <button
              onClick={(e) => {
                e.preventDefault();
                updateStatus(false);
                setPreColor();
              }}
              className="rgb-button"
            >
              Cancel
            </button>
          </div>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default RgbPanel;
