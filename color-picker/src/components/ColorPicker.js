import { useEffect, useState, useRef } from "react";
import "./ColorPicker.css";
import RgbPanel from "./RgbPanel";
import ColorBase from "./ColorBase";

const ColorPicker = ({ value, onChange, colors }) => {
  const [color, setColor] = useState(value);
  const [opened, setOpened] = useState(false);
  const [colorBaseStatus, setColorBaseStatus] = useState(false);

  const menuRef = useRef();
  const colorBaseRef = useRef();

  useEffect(() => {
    onChange(color);

    document.querySelector("#color-square").style.backgroundColor = color;
  }, [color]);

  useEffect(() => {
    if (opened) {
      window.addEventListener("mousedown", closeMenu);
    }
  });

  useEffect(() => {
    if (colorBaseStatus) {
      window.addEventListener("mousedown", closeColorBase, { once: true });
    }
  });

  const closeMenu = (e) => {
    if (!menuRef.current.contains(e.target)) {
      setOpened(false);
      document.querySelector("#color-square").style.backgroundColor = color;
      window.removeEventListener("mousedown", closeMenu);
    }
  };

  const closeColorBase = (e) => {
    if (!colorBaseRef.current.contains(e.target)) {
      setColorBaseStatus(false);
    }
  };

  const changeColor = () => {
    setColor(document.querySelector("#color-picker-input").value);
    onChange(document.querySelector("#color-picker-input").value);
  };

  const openWindow = () => {
    setOpened(true);
  };

  const openColorBase = () => {
    setColorBaseStatus(true);
  };

  const getRgbData = (e) => {
    setColor(e);
    onChange(e);
  };

  const setPreColor = () => {
    document.querySelector("#color-square").style.backgroundColor = color;
  };

  const returnStatus = (status) => {
    setOpened(status);
    window.removeEventListener("mousedown", closeMenu);
  };

  const returnColorBaseStatus = (status) => {
    setColorBaseStatus(status);
  };

  return (
    <div className="color-picker-area">
      <input
        id="color-picker-input"
        placeholder="hex here"
        value={color}
        onChange={changeColor}
      />
      <div>
        <div id="color-square" onClick={openWindow} />
        <div className="rgb-area">
          <div id="rgb-panel" ref={menuRef}>
            <RgbPanel
              getData={getRgbData}
              status={opened}
              updateStatus={returnStatus}
              setPreColor={setPreColor}
            />
          </div>
        </div>
      </div>
      <div className="color-base-area">
        <button onClick={openColorBase}>&#9660;</button>
        <div className="color-base-panel" ref={colorBaseRef}>
          <ColorBase
            colors={colors}
            status={colorBaseStatus}
            getColor={getRgbData}
            updateStatus={returnColorBaseStatus}
          />
        </div>
      </div>
    </div>
  );
};

export default ColorPicker;
