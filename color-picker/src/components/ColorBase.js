import "./ColorBase.css";

const ColorBase = ({ colors, status, getColor, updateStatus }) => {
  return (
    <div className="color-base-area">
      {status ? (
        <ul>
          {colors.map((color) => (
            <li key={color.colorName}>
              <button
                onClick={(e) => {
                  e.preventDefault();
                  getColor(color.colorHex);
                  updateStatus(false);
                }}
              >
                {color.colorName}{" "}
                <div
                  className="color-square"
                  style={{ backgroundColor: color.colorHex }}
                ></div>
              </button>
            </li>
          ))}
        </ul>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default ColorBase;
