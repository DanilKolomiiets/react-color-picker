import { useEffect, useState } from "react";
import "./App.css";
import ColorPicker from "./components/ColorPicker";

function App() {
  const [color, setColor] = useState("#BB3211");

  useEffect(() => {
    document.querySelector("#color-picker-input").value = color;
  }, [color]);

  const changeColor = (e) => {
    setColor(e);
  };

  let colors = [
    {
      colorName: "Green",
      colorHex: "#23a13a",
    },
    {
      colorName: "Red",
      colorHex: "#ff0000",
    },
    {
      colorName: "Blue",
      colorHex: "#0800ff",
    },
  ];

  return (
    <div className="App">
      <ColorPicker
        id="color-picker"
        value={color}
        onChange={changeColor}
        colors={colors}
      />
    </div>
  );
}

export default App;
